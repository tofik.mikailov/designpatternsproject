package adapter;

public class Java {

    public void insertObject(){
        System.out.println("Saving Java object");
    }

    public void updateObject(){
        System.out.println("Updating Java object");
    }

    public void deleteObject(){
        System.out.println("Deleting Java object");
    }

    public void loadObject(){
        System.out.println("Loading Java object");
    }
}
