package adapter;

public class Adapter extends Java implements Database {
    @Override
    public void insert() {
        insertObject();
    }

    @Override
    public void update() {
        updateObject();
    }

    @Override
    public void remove() {
        deleteObject();
    }

    @Override
    public void select() {
        loadObject();
    }
}
