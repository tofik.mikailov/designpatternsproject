package singleton;

public class SimpleSingleton {
    private static final SimpleSingleton SIMPLE_SINGLETON=new SimpleSingleton();

    private SimpleSingleton(){

    }

    public static SimpleSingleton getSimpleSingleton() {
        return SIMPLE_SINGLETON;
    }
}
