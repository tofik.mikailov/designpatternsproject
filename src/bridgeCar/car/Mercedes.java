package bridgeCar.car;

import bridgeCar.color.Color;

public class Mercedes extends Car {
    public Mercedes(Color color) {
        super(color);
    }

    @Override
    public void setColor() {
        System.out.print("Mercedes color is ");
        getColor().setColor();
    }
}
