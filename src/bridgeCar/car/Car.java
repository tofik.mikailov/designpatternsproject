package bridgeCar.car;

import bridgeCar.color.Color;

public abstract class Car {
    private Color color;

    public Car(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public abstract void setColor();
}
