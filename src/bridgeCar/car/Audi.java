package bridgeCar.car;

import bridgeCar.color.Color;

public class Audi extends Car {

    public Audi(Color color) {
        super(color);
    }

    @Override
    public void setColor() {
        System.out.print("Audi color is ");
        getColor().setColor();
    }
}
