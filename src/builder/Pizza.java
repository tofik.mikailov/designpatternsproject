package builder;

public class Pizza {
    private int countOfSausage;
    private int countOfCheese;
    private int countOfOnion;
    private int countOfMushrooms;

    public int getCountOfSausage() {
        return countOfSausage;
    }

    public void setCountOfSausage(int countOfSausage) {
        this.countOfSausage = countOfSausage;
    }

    public int getCountOfCheese() {
        return countOfCheese;
    }

    public void setCountOfCheese(int countOfCheese) {
        this.countOfCheese = countOfCheese;
    }

    public int getCountOfOnion() {
        return countOfOnion;
    }

    public void setCountOfOnion(int countOfOnion) {
        this.countOfOnion = countOfOnion;
    }

    public int getCountOfMushrooms() {
        return countOfMushrooms;
    }

    public void setCountOfMushrooms(int countOfMushrooms) {
        this.countOfMushrooms = countOfMushrooms;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "countOfSausage=" + countOfSausage +
                ", countOfCheese=" + countOfCheese +
                ", countOfOnion=" + countOfOnion +
                ", countOfMushrooms=" + countOfMushrooms +
                '}';
    }

    public static class Builder {
        Pizza pizza;

        public Builder() {
        pizza = new Pizza();
        }

        public Builder setCountOfSausage(int countOfSausage) {
            pizza.countOfSausage=countOfSausage;
            return this;
        }

        public Builder setCountOfCheese(int countOfCheese) {
            pizza.countOfCheese = countOfCheese;
            return this;
        }

        public Builder setCountOfOnion(int countOfOnion) {
            pizza.countOfOnion = countOfOnion;
            return this;
        }

        public Builder setCountOfMushrooms(int countOfMushrooms) {
            pizza.countOfMushrooms = countOfMushrooms;
            return this;
        }

        public Pizza build(){
            return pizza;
        }
    }
}
