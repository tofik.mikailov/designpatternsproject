package bridge.remotes;

public interface Remote {
    void volumeUp();
    void volumeDown();
    void channelUp();
    void channelDown();
    void power();
}
