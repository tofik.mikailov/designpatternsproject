package bridge.remotes;

import bridge.devices.Device;

public class BasicRemote implements Remote {

    Device device;

    public BasicRemote(Device device) {
        this.device = device;
    }

    @Override
    public void volumeUp() {
        System.out.println("Changing volume up");
        device.setVolume(device.getVolume()+10);
    }

    @Override
    public void volumeDown() {
        System.out.println("Changing volume down");
        device.setVolume(device.getVolume()-10);
    }

    @Override
    public void channelUp() {
        System.out.println("Changing channel up");
        device.setChannel(device.getChannel()+1);
    }

    @Override
    public void channelDown() {
        System.out.println("Changing channel down");
        device.setChannel(device.getChannel()-1);
    }

    @Override
    public void power() {
    if (device.isEnabled()){
        device.disable();
    } else {
        device.enable();
    }
    }
}
