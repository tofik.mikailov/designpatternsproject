import adapter.Adapter;
import adapter.Database;
import bridge.devices.Device;
import bridge.devices.Radio;
import bridge.devices.Tv;
import bridge.remotes.AdvancedRemote;
import bridge.remotes.BasicRemote;
import bridgeCar.car.Audi;
import bridgeCar.car.Mercedes;
import bridgeCar.color.BlueColor;
import bridgeCar.color.RedColor;
import builder.Pizza;
import factory.Person;
import factory.PersonFactory;
import factory.TofikFactory;
import factory.TuralFactory;
import factory2.GetDiscount;
import factory2.enums.SuperMarketEnum;
import prototype.Car;
import singleton.SimpleSingleton;
import singletonLazy.SingletonLazy;

import java.math.BigDecimal;

import static singleton.SimpleSingleton.getSimpleSingleton;
import static singletonLazy.SingletonLazy.getSingletonLazy;

public class Main {

    public static void main(String[] args) {
        //Factory Method
        PersonFactory personFactory = createPersonFactory("Tofik");
        Person person = personFactory.createPerson();
        person.sayMyName();
        System.out.println("\n========================");
        //Prototype
        Car firstCar = new Car(4, 4, 3500);
        Car secondCar = firstCar.copyOfCar();
        System.out.println(firstCar == secondCar);
        System.out.println("\n========================");
        //Builder
        Pizza pizza = new Pizza.Builder()
                .setCountOfCheese(12)
                .setCountOfMushrooms(3)
                .setCountOfSausage(2)
                .setCountOfOnion(7).build();
        System.out.println(pizza);
        System.out.println("\n========================");
        //Builder 2 by Kamil Humbatov
        BigDecimal total = new BigDecimal("78.5");
        System.out.println("Araz discount total is " + new GetDiscount().calc(SuperMarketEnum.ARAZ, total));
        System.out.println("Bravo discount total is " + new GetDiscount().calc(SuperMarketEnum.BRAVO, total));
        System.out.println("Favorit discount total is " + new GetDiscount().calc(SuperMarketEnum.FAVORIT, total));
        System.out.println("\n========================");
        //Simple Singleton by Tofik Mikailzade
        SimpleSingleton simpleSingleton = getSimpleSingleton();
        System.out.println("\n========================");
        //Lazy Singleton by Tofik Mikailzade
        SingletonLazy singletonLazy = getSingletonLazy();
        System.out.println("\n========================");
        //Adapter by Tofik Mikailzade
        Database database = new Adapter();
        database.insert();
        database.remove();
        database.select();
        database.update();
        System.out.println("\n========================");
        //Bridge by Tofik Mikailzade
        testRemote(new Radio());
        testRemote(new Tv());
        System.out.println("\n========================");
        //Bridge2 by Tofik Mikailzade
        bridgeCar.car.Car audi = new Audi(new RedColor());
        audi.setColor();
        bridgeCar.car.Car mercedes = new Mercedes(new BlueColor());
        mercedes.setColor();

    }


    public static PersonFactory createPersonFactory(String name) {
        if (name.equalsIgnoreCase("tofik")) {
            return new TofikFactory();
        }else if(name.equalsIgnoreCase("TURAL")){
            return new TuralFactory();
        } else {
            throw new RuntimeException(name + "type of factory not found");
        }
    }

    public static void testRemote(Device device){
        System.out.println("Test with basic remote");
        BasicRemote basicRemote=new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Test with advanced remote");
        AdvancedRemote advancedRemote=new AdvancedRemote(device);
        advancedRemote.power();
        device.printStatus();
    }
}
