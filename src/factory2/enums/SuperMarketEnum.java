package factory2.enums;

public enum SuperMarketEnum {

    FAVORIT("Favorit"),ARAZ("Araz"),BRAVO("Bravo");

    private String description;

    private SuperMarketEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
