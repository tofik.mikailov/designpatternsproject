package factory2;


import factory2.enums.SuperMarketEnum;
import factory2.model.Discount;

import java.math.BigDecimal;

public class GetDiscount {
    public BigDecimal calc(SuperMarketEnum superMarketEnum, BigDecimal total) {
        Discount market = new GetSuperMarketFactory().get(superMarketEnum);
        if (market != null) {
            return market.calc(total);
        }
        return BigDecimal.ZERO;
    }
}
