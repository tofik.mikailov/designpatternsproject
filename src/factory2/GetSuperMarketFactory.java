package factory2;

import factory2.enums.SuperMarketEnum;
import factory2.model.Araz;
import factory2.model.Bravo;
import factory2.model.Discount;
import factory2.model.Favorit;

public class GetSuperMarketFactory {

    public Discount get(SuperMarketEnum superMarket) {
        if (superMarket == null) {
            return null;
        }
        if (SuperMarketEnum.ARAZ.compareTo(superMarket) == 0) {
            return new Araz();
        } else if (SuperMarketEnum.BRAVO.compareTo(superMarket) == 0) {
            return new Bravo();
        } else if (SuperMarketEnum.FAVORIT.compareTo(superMarket) == 0) {
            return new Favorit();
        } else {
            return null;
        }
    }
}
