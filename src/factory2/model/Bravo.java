package factory2.model;

import factory2.util.CalcPercent;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Bravo extends Discount {

    private static final BigDecimal discountEven = BigDecimal.valueOf(10);
    private static final BigDecimal discountOdd = BigDecimal.valueOf(5);

    public BigDecimal calc(BigDecimal total) {
        switch (LocalDate.now().getDayOfWeek()) {
            case MONDAY:
            case WEDNESDAY:
            case FRIDAY:
            case SUNDAY:
                return CalcPercent.calc(total, discountOdd);
            case TUESDAY:
            case THURSDAY:
            case SATURDAY:
                return CalcPercent.calc(total, discountEven);
        }
        return BigDecimal.ZERO;
    }
}
