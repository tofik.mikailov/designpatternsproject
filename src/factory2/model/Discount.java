package factory2.model;

import java.math.BigDecimal;

public abstract class Discount {

    public abstract BigDecimal calc(BigDecimal total);
}
