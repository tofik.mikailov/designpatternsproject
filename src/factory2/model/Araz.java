
package factory2.model;

import factory2.util.CalcPercent;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;

public class Araz extends Discount {

    private static final BigDecimal discount = BigDecimal.valueOf(15);
    private static final BigDecimal discountSaturday = BigDecimal.valueOf(20);

    public BigDecimal calc(BigDecimal total) {
        if (LocalDate.now().getDayOfWeek().compareTo(DayOfWeek.SATURDAY) == 0) {
            return CalcPercent.calc(total, discountSaturday);
        } else {
            return CalcPercent.calc(total, discount);
        }
    }
}
