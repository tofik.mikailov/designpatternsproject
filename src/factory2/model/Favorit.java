
package factory2.model;

import factory2.util.CalcPercent;

import java.math.BigDecimal;

public class Favorit extends Discount {

    private static final BigDecimal discount = BigDecimal.valueOf(20);

    public BigDecimal calc(BigDecimal total) {
        return CalcPercent.calc(total, discount);
    }
}