package factory2.util;

import java.math.BigDecimal;

public class CalcPercent {

    public static BigDecimal calc(BigDecimal total, BigDecimal percent) {
        return total.multiply(percent).divide(new BigDecimal(100));
    }
}
