package prototype;

public interface Copyable {
    Car copyOfCar();
}
