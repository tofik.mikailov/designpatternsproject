package prototype;

public class Car implements Copyable {
    private int countOfWheels;
    private int countOfDoors;
    private int motorVolume;

    public Car(int countOfWheels, int countOfDoors, int motorVolume) {
        this.countOfWheels = countOfWheels;
        this.countOfDoors = countOfDoors;
        this.motorVolume = motorVolume;
    }

    public int getCountOfWheels() {
        return countOfWheels;
    }

    public void setCountOfWheels(int countOfWheels) {
        this.countOfWheels = countOfWheels;
    }

    public int getCountOfDoors() {
        return countOfDoors;
    }

    public void setCountOfDoors(int countOfDoors) {
        this.countOfDoors = countOfDoors;
    }

    public int getMotorVolume() {
        return motorVolume;
    }

    public void setMotorVolume(int motorVolume) {
        this.motorVolume = motorVolume;
    }

    @Override
    public Car copyOfCar() {
        return new Car(countOfWheels,countOfDoors,motorVolume);
    }
}
