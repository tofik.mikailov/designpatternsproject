package factory;

public interface PersonFactory {
    Person createPerson();
}
