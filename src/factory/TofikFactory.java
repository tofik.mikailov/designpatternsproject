package factory;

public class TofikFactory implements PersonFactory {
    @Override
    public Person createPerson() {
        return new Tofik();
    }
}
