package factory;

public class TuralFactory implements PersonFactory {
    @Override
    public Person createPerson() {
        return new Tural();
    }
}
