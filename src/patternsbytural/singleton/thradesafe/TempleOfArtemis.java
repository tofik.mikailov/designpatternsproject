package patternsbytural.singleton.thradesafe;

public class TempleOfArtemis {
    private TempleOfArtemis(){ }
    private static TempleOfArtemis templeOfArtemis;
    public synchronized static TempleOfArtemis getInstance(){
        if(templeOfArtemis==null){
            templeOfArtemis=new TempleOfArtemis();
        }
        return templeOfArtemis;
    }
}
