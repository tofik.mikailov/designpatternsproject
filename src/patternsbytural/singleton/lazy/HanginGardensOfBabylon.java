package patternsbytural.singleton.lazy;

public class HanginGardensOfBabylon {
    private static HanginGardensOfBabylon hanginGardensOfBabylon;
    private HanginGardensOfBabylon(){}
    public static  HanginGardensOfBabylon getInstance(){
        if(hanginGardensOfBabylon==null) {
            hanginGardensOfBabylon = new HanginGardensOfBabylon();
        }
        return hanginGardensOfBabylon;
    }
}
