package patternsbytural.singleton.billpughsingleton;

public class ColossusOfRhodes {
    private ColossusOfRhodes(){}

    private static class BillPughSingleton{
        private static final ColossusOfRhodes COLOSSUS_OF_RHODES=new ColossusOfRhodes();
    }
    public static ColossusOfRhodes getInstance(){
        return BillPughSingleton.COLOSSUS_OF_RHODES;
    }
}
