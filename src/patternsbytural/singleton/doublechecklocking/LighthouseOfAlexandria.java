package patternsbytural.singleton.doublechecklocking;

public class LighthouseOfAlexandria {
    private volatile static LighthouseOfAlexandria lighthouseOfAlexandria;

    private LighthouseOfAlexandria(){

    }
    public static LighthouseOfAlexandria getInstance(){
        if(lighthouseOfAlexandria==null){
            synchronized (LighthouseOfAlexandria.class){
                if(lighthouseOfAlexandria==null){
                    lighthouseOfAlexandria=new LighthouseOfAlexandria();
                }
            }
        }
        return lighthouseOfAlexandria;
    }
}
