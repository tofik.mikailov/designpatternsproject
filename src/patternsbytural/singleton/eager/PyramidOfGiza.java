package patternsbytural.singleton.eager;

public class PyramidOfGiza {
    private PyramidOfGiza() {
    }

    private static final PyramidOfGiza pyramidOfGiza=new PyramidOfGiza();

    public static PyramidOfGiza getInstance() {
        return pyramidOfGiza;
    }
}
