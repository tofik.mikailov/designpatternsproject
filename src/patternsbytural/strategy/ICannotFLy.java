package patternsbytural.strategy;

public class ICannotFLy implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println(" - I can't fly");
    }
}
