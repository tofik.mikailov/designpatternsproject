package patternsbytural.strategy;

public class Batman extends SuperHero {
    public Batman(String name, String species) {
        super(name, species);
        flyBehavior=new ICanFlyWithHighTech();
        speedBehavior =new IDontHaveSuperSpeed();
    }
}
