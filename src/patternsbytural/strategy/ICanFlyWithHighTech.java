package patternsbytural.strategy;

public class ICanFlyWithHighTech implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println(" - I am flyin by using hightech");
    }
}
