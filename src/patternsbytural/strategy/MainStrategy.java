package patternsbytural.strategy;

public class MainStrategy {
    public static void main(String[] args) {
        SuperHero batman=new Batman("Batman","Human");
        batman.display();
        batman.performAbilities();
        SuperHero toyBatman=new ToyBatman("ToyBatman","Plastic");
        toyBatman.display();
        toyBatman.performAbilities();
    }
}
