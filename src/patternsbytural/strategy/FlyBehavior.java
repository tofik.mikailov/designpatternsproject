package patternsbytural.strategy;

public interface FlyBehavior {
    public abstract void fly();
}
