package patternsbytural.strategy;

public class IHaveSuperSpeed implements SuperSpeedBehavior{

    @Override
    public void speed() {
        System.out.println(" - Look how fast i am");
    }
}
