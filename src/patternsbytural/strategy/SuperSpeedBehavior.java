package patternsbytural.strategy;

public interface SuperSpeedBehavior {
    public abstract void speed();

}
