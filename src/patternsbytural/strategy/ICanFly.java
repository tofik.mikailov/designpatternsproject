package patternsbytural.strategy;

public class ICanFly implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println(" - I am flying");
    }
}
