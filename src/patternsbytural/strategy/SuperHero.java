package patternsbytural.strategy;

public abstract class SuperHero {
    private String name;
    private String species;
    protected FlyBehavior flyBehavior;
    protected SuperSpeedBehavior speedBehavior;

    public SuperHero(String name, String species) {
        if(name != null)
            this.name = name;
        if(species !=null)
            this.species = species;
    }
public void performAbilities(){
        flyBehavior.fly();
        speedBehavior.speed();

}
    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setSpeedBehavior(SuperSpeedBehavior speedBehavior) {
        this.speedBehavior = speedBehavior;
    }
    public void display(){
        System.out.println(" Name = { "+getName()+"} Species = { "+getSpecies()+" } ");
    }
}

