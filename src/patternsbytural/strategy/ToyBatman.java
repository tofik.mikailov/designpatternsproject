package patternsbytural.strategy;

public class ToyBatman extends SuperHero {
    public ToyBatman(String name, String species) {
        super(name, species);
        flyBehavior = new ICannotFLy();
        speedBehavior=()-> System.out.println(" - I'm just a toy!Toys do not have any speed.");

    }
}
