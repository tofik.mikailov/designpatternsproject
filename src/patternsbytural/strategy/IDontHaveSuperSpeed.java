package patternsbytural.strategy;

public class IDontHaveSuperSpeed implements SuperSpeedBehavior {
    @Override
    public void speed() {
        System.out.println(" - I don't have a superspeed!I'm not Flash");
    }
}
