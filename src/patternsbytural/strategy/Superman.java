package patternsbytural.strategy;

public class Superman extends SuperHero {
    public Superman(String name, String species) {
        super(name, species);
        flyBehavior=new ICanFly();
        speedBehavior =new IHaveSuperSpeed();

    }
}
