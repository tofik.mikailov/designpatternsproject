package patternsbytural.strategy;

public class ToySuperman extends SuperHero {
    public ToySuperman(String name, String species) {
        super(name, species);
        flyBehavior=new ICannotFLy();
        speedBehavior=()-> System.out.println(" - I'm just a toy!Toys do not have any speed.");
        }
    }

