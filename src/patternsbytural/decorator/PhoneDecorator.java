package patternsbytural.decorator;

public abstract class PhoneDecorator extends Phone {
    @Override
    public abstract String getPhoneModel();
}
