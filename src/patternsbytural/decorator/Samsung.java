package patternsbytural.decorator;

public class Samsung  extends Phone{

    @Override
    public double cost() {
        return 250;
    }

    public Samsung() {
        phoneModel="Samsung";
    }
}
