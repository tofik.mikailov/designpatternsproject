package patternsbytural.decorator;

public abstract class Phone {
    protected  String phoneModel="Unknown";

    public String getPhoneModel() {
        return phoneModel;
    }
    public abstract double cost();
}
