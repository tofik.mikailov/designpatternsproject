package patternsbytural.decorator;

public class DisplayPhoneInfo implements Displayable {
    @Override
    public void display(Phone phone) {
        System.out.println("Brand new "+phone.getPhoneModel()+" just for $ "+phone.cost());
    }
}
