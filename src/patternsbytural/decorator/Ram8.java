package patternsbytural.decorator;

public class Ram8  extends PhoneDecorator{
   private Phone phone;

    public Ram8(Phone phone) {
        this.phone = phone;
    }

    @Override
    public String getPhoneModel() {
        return phone.getPhoneModel()+" ,with 8 GB RAM ";
    }

    @Override
    public double cost() {
        return phone.cost()+50;
    }
}
