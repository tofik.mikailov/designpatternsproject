package patternsbytural.decorator;

public interface Displayable {
    public void display(Phone phone);

}
