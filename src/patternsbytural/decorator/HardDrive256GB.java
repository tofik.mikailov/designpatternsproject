package patternsbytural.decorator;

public class HardDrive256GB extends PhoneDecorator {
    private  Phone phone;

    public HardDrive256GB(Phone phone) {
        this.phone = phone;
    }

    @Override
    public String getPhoneModel() {
        return phone.getPhoneModel()+" ,with 256 GB HardDrive";
    }

    @Override
    public double cost() {
        return phone.cost()+100;
    }
}
