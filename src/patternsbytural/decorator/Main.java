package patternsbytural.decorator;

public class Main {
    public static void main(String[] args) {
        Displayable displayPhoneInfo = new DisplayPhoneInfo();
        Phone iphone = new HardDrive256GB(new Ram8(new Iphone()));
        displayPhoneInfo.display(iphone);
    }
}
