package patternsbytural.decorator;

public class Iphone  extends Phone{
    @Override
    public double cost() {
        return 300;
    }

    public Iphone() {
        phoneModel="Iphone";
    }
}
