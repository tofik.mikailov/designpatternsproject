package patternsbytural.decorator;

public class Ram12 extends PhoneDecorator {
   private Phone phone;

    public Ram12(Phone phone) {
        this.phone = phone;
    }

    @Override
    public String getPhoneModel() {
        return phone.getPhoneModel()+",with 12 GB RAM";
    }

    @Override
    public double cost() {
        return phone.cost()+ 100;
    }
}
