package patternsbytural.decorator;

public class HardDrive64Gb  extends PhoneDecorator{
    private Phone phone;
    @Override
    public String getPhoneModel() {
        return phone.getPhoneModel()+" ,with 64 GB Hard Drive ";
    }

    @Override
    public double cost() {
        return phone.cost()+50;
    }

    public HardDrive64Gb(Phone phone) {
        this.phone = phone;
    }
}
