package patternsbytural.observer;

import java.time.LocalDate;

public class MainObserver {
    public static void main(String[] args) {
        UniversalPictures universalPictures=new UniversalPictures();
        ParkCinema parkCinema=new ParkCinema(universalPictures);
        BakuCinema bakuCinema=new BakuCinema(universalPictures);
        universalPictures.setMovieTitleAndReleaseDate("Avengers", LocalDate.of(2020,11,20));

    }
}
