package patternsbytural.observer;

public interface Observer {
    public abstract void update();
}
