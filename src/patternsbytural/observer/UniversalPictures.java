package patternsbytural.observer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UniversalPictures implements Observable {
    private String movieTitle = null;
    private LocalDate dateOfRelease = null;
    private List<Observer> observers;

    public UniversalPictures() {
        observers = new ArrayList<Observer>();
    }

    public void setMovieTitleAndReleaseDate(String movieTitle, LocalDate dateOfRelease) {
        this.movieTitle = movieTitle;
        this.dateOfRelease = dateOfRelease;
        notifyAllObservers();
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public LocalDate getDateOfRelease() {
        return dateOfRelease;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        int i = observers.indexOf(observer);
        if (i >= 0) {
            observers.remove(i);
        }
    }

    @Override
    public void notifyAllObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
