package patternsbytural.observer;

public interface Observable {
    public abstract void addObserver(Observer observer);
    public abstract void removeObserver(Observer observer);
    public void notifyAllObservers();
}
