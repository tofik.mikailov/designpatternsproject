package patternsbytural.observer;

public interface Displayable {
    public abstract void display();
}
