package patternsbytural.observer;

public class ParkCinema implements Observer, Displayable {
    private final String cinemaName = "Park Cinema";
    private UniversalPictures universalPictures;

    public ParkCinema(UniversalPictures universalPictures) {
        this.universalPictures = universalPictures;
        universalPictures.addObserver(this);
    }

    @Override
    public void update() {
        display();
    }

    @Override
    public void display() {
        if (universalPictures.getDateOfRelease() == null) {
            System.out.println("Movie title is :" + universalPictures.getMovieTitle() + " coming soon in " + cinemaName);
        }
        if (universalPictures.getMovieTitle() == null) {
            System.out.println("Surpise movie coming on " + universalPictures.getDateOfRelease() + " " + cinemaName);
        }
        if (universalPictures.getDateOfRelease() != null && universalPictures.getMovieTitle() != null) {
            System.out.println("Movie :" + universalPictures.getMovieTitle() + " coming in  " + universalPictures.getDateOfRelease() +
                    " get your tickets in " + cinemaName);
        }
    }
}
