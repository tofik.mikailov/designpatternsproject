package patternsbytural.observer;

public class BakuCinema implements Observer, Displayable {
    private final String cinemaName = "Baku Cinema";
    private UniversalPictures universalPictures;

    public BakuCinema(UniversalPictures observable) {
        this.universalPictures = observable;
        observable.addObserver(this);
    }

    @Override
    public void display() {
        if (universalPictures.getDateOfRelease() == null) {
            System.out.println("Movie title is :" + universalPictures.getMovieTitle() + " coming soon in " + cinemaName);
        }
        if (universalPictures.getMovieTitle() == null) {
            System.out.println("Surpise movie coming on " + universalPictures.getDateOfRelease() + " " + cinemaName);
        }
        if (universalPictures.getDateOfRelease() != null && universalPictures.getMovieTitle() != null) {
            System.out.println("Movie :" + universalPictures.getMovieTitle() + " coming in  " + universalPictures.getDateOfRelease() +
                    " get your tickets in " + cinemaName);
        }
    }

    @Override
    public void update() {
        display();
    }
}
