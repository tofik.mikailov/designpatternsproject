package patternsbytural.factories.simplefactoryidiom;

public class HamBurger extends Burger {
    public HamBurger() {
        name = "Hamburger";
        toppings.add("Pickles");
        toppings.add("Tomatoes");
    }
}
