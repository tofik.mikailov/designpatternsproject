package patternsbytural.factories.simplefactoryidiom;

public class SimpleMcdonaldsFactory {
    public Burger createBurger(String type) {
        Burger burger = null;
        if (type.equalsIgnoreCase("cheeseBurger")) {
            burger = new CheeseBurger();
        } else if (type.equalsIgnoreCase("hamburger")) {
            burger = new HamBurger();
        } else {
            throw new RuntimeException("There is no such burger" + type);
        }
        return burger;
    }
}
