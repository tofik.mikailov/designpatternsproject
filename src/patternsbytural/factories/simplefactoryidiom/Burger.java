package patternsbytural.factories.simplefactoryidiom;

import java.util.ArrayList;
import java.util.List;

public abstract class Burger {
    protected String name;
    protected List<String> toppings = new ArrayList<String>();
    protected String sauce = " - Mcdonads special sauce";

    public void prepareBurger() {
        System.out.println("Preparing " + name);
        System.out.println("Adding sauce : \n" + sauce);
        System.out.println("Adding toppings");
        for (String s : toppings) {
            System.out.println(" - " + s);
        }
    }

    public void wrapBurger() {
        System.out.println("Wrapping " + name);
    }

    @Override
    public String toString() {
        return name + sauce;
    }

    public String getName() {
        return name;
    }
    public void dislay(){
        System.out.println("Your order "+getName()+" ready! Bone Appetit!!!");
    }
}

