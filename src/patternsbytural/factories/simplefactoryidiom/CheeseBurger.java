package patternsbytural.factories.simplefactoryidiom;

public class CheeseBurger extends Burger {
    public CheeseBurger() {
        name = "CheeseBurger";

        toppings.add("Cheese");
        toppings.add("Pickles");

    }
}
