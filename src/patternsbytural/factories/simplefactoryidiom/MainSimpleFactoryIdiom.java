package patternsbytural.factories.simplefactoryidiom;

public class MainSimpleFactoryIdiom {
    public static void main(String[] args) {
        SimpleMcdonaldsFactory factory = new SimpleMcdonaldsFactory();
        Mcdonals mcdonals = new Mcdonals(factory);
        Burger cheeseburger = mcdonals.orderBurger("cheeseburger");
        cheeseburger.dislay();
        Burger hamburger=mcdonals.orderBurger("hamburger");
        hamburger.dislay();


    }
}
