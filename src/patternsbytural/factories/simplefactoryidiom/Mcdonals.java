package patternsbytural.factories.simplefactoryidiom;

public class Mcdonals {
    SimpleMcdonaldsFactory simpleMcdonaldsFactory;

    public Mcdonals(SimpleMcdonaldsFactory simpleMcdonaldsFactory) {
        this.simpleMcdonaldsFactory = simpleMcdonaldsFactory;
    }

    public Burger orderBurger(String type) {
        Burger burger = simpleMcdonaldsFactory.createBurger(type);
        burger.prepareBurger();
        burger.wrapBurger();
        return burger;

    }
}
