package patternsbytural.factories.factorymethod;

public class NYStyleHamburger extends  Burger{
    public NYStyleHamburger() {
        name="New York Hamburger";
        meatType="Beef + Pork";
        toppings.add("pickles");
        toppings.add("tomato");

    }
}
