package patternsbytural.factories.factorymethod;


public abstract class McdonaldsFactory {
       protected abstract Burger createBurger(String type);
    public Burger orderBurger(String type) {
        Burger burger = createBurger(type);
        burger.prepareBurger();
        burger.wrapBurger();
        return burger;

    }
}
