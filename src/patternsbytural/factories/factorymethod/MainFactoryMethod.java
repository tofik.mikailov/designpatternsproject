package patternsbytural.factories.factorymethod;

public class MainFactoryMethod {
    public static void main(String[] args) {
        McdonaldsFactory bakuCity=new BakuCityMcdonaldsFactory();
        McdonaldsFactory nyCity=new NYCityMcdonaldsFactory();

        Burger burgerbaku=bakuCity.orderBurger("cheeseburger");
        burgerbaku.dislay();

    }
}
