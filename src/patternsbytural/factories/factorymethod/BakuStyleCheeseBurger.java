package patternsbytural.factories.factorymethod;

public class BakuStyleCheeseBurger extends Burger{
    public BakuStyleCheeseBurger() {
        name="Baku Style CheeseBurger";
        meatType="Beef";
        toppings.add("picles");
        toppings.add("cheese");
        toppings.add("lettuce");
    }
}
