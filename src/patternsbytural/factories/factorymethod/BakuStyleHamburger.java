package patternsbytural.factories.factorymethod;

public class BakuStyleHamburger extends Burger {
    public BakuStyleHamburger() {
        name="Baku Style Hamburger";
        meatType="Beef";
        toppings.add("pickles");
        toppings.add("tomato");
    }
}
