package patternsbytural.factories.factorymethod;

public class NYStyleCheeseBurger extends Burger {
    public NYStyleCheeseBurger() {
        name="New York Cheese Burger";
        meatType="Beef + Pork";
        toppings.add("cheese");
        toppings.add("pickles");
    }
}
