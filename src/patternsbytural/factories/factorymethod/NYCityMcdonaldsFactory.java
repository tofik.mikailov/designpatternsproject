package patternsbytural.factories.factorymethod;

public class NYCityMcdonaldsFactory extends McdonaldsFactory {
    @Override
    protected Burger createBurger(String type) {
        if (type.equalsIgnoreCase("CheeseBurger")) {
            return new NYStyleCheeseBurger();
        } else if (type.equalsIgnoreCase("hamburger")) {
            return new NYStyleHamburger();
        } else {
            throw new RuntimeException("There is no such burger" + type);
        }

    }
}
