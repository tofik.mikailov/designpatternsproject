package patternsbytural.factories.factorymethod;

public class BakuCityMcdonaldsFactory extends McdonaldsFactory {

    @Override
    protected Burger createBurger(String type) {
        if (type.equalsIgnoreCase("cheeseBurger")) {
            return new BakuStyleCheeseBurger();
        }else if (type.equalsIgnoreCase("hamburger")) {
            return new BakuStyleHamburger();
        } else {
           throw new RuntimeException("There is no such burger" + type);
        }

    }
}
