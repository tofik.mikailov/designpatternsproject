package patternsbytural.factories.abstractfactory;

public class Beef implements Meat {
    @Override
    public String toString() {
        return "Beef";
    }
}
