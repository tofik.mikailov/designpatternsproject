package patternsbytural.factories.abstractfactory;

public class Tomato implements Veggies {
    @Override
    public String toString() {
        return "Tomato";
    }
}
