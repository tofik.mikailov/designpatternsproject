package patternsbytural.factories.abstractfactory;

public class MozarellaCheese implements Cheese {
    @Override
    public String toString() {
        return "Mozarella Cheese";
    }
}
