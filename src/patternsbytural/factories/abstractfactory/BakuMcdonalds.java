package patternsbytural.factories.abstractfactory;

public class BakuMcdonalds extends Mcdonalds {
    @Override
    protected Burger createBurger(String type) {
        Burger burger = null;
        McdonaldsBurgerFactory bakufactory = new BakuCityMcdonaldsBurgerFactory();
        if (type.equalsIgnoreCase("cheeseburger")) {
            burger = new CheeseBurger(bakufactory);
            burger.setName("Baku Style Cheese Burger");
        } else if (type.equalsIgnoreCase("hamburger")) {
            burger = new Hamburger(bakufactory);
            burger.setName("Baku Style Hamburger");
        } else {
            throw new RuntimeException("There is no such burger" + type);
        }
        return burger;
    }
}
