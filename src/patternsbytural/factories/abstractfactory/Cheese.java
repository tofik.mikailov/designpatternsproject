package patternsbytural.factories.abstractfactory;

public interface Cheese {
    public String toString();
}
