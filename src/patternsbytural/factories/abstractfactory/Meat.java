package patternsbytural.factories.abstractfactory;

public interface Meat {
    public abstract String toString();
}
