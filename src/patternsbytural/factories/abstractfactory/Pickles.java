package patternsbytural.factories.abstractfactory;

public class Pickles implements Veggies {
    @Override
    public String toString() {
        return "pickles";
    }
}
