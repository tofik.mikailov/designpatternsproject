package patternsbytural.factories.abstractfactory;

public class NYMcdonalds extends Mcdonalds {
    @Override
    protected Burger createBurger(String type) {
        Burger burger=null;
        McdonaldsBurgerFactory nyCityFactory=new NYCityMcdonaldsBurgerFactory();
       if(type.equalsIgnoreCase("cheeseBurger")){
           burger=new CheeseBurger(nyCityFactory);
           burger.setName("New york style Cheeseburger");
       }else if (type.equalsIgnoreCase("hamburger")){
           burger=new Hamburger(nyCityFactory);
           burger.setName("New York style Hamburger");
       }else {
           throw new RuntimeException("There is no such burger "+type);
       }
        return burger;
    }
}
