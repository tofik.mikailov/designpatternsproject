package patternsbytural.factories.abstractfactory;

public class CheddarCheese implements Cheese {
    @Override
    public String toString() {
        return "Cheddar Cheese";
    }
}
