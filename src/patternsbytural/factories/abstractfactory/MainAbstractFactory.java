package patternsbytural.factories.abstractfactory;

public class MainAbstractFactory {
    public static void main(String[] args) {
        Mcdonalds bakuMcdonalds = new BakuMcdonalds();
        Mcdonalds nyMcdonalds = new NYMcdonalds();

        Burger burger = bakuMcdonalds.orderBurger("CheeseBurger");
        burger.dislay();
        Burger hamburger=nyMcdonalds.orderBurger("hamburger");
        hamburger.dislay();


    }
}
