package patternsbytural.factories.abstractfactory;

import java.util.Arrays;

public class Hamburger extends Burger {
    McdonaldsBurgerFactory mcdonaldsBurgerFactory;

    public Hamburger(McdonaldsBurgerFactory mcdonaldsBurgerFactory) {
        this.mcdonaldsBurgerFactory = mcdonaldsBurgerFactory;
    }

    @Override
    public void prepareBurger() {
        System.out.println("Preparing \n - " + name);
        System.out.println("Adding sauce : \n - " + sauce);

        cheese = mcdonaldsBurgerFactory.createCheese();
        System.out.println("Adding Cheese  \n - "+cheese);

        meat = mcdonaldsBurgerFactory.createMeat();
        System.out.println("Grilling meat \n - "+meat);

        veggies = mcdonaldsBurgerFactory.createVeggies();
        System.out.println("Adding toppings \n - "+ Arrays.toString(veggies));

    }
}
