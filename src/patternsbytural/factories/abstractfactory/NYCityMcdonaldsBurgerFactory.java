package patternsbytural.factories.abstractfactory;

public class NYCityMcdonaldsBurgerFactory implements McdonaldsBurgerFactory {
    @Override
    public Meat createMeat() {
        return new Pork();
    }

    @Override
    public Cheese createCheese() {
        return new CheddarCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies={new Lettuce(),new Pickles(),new Onion(),new Tomato()};
        return veggies;
    }
}
