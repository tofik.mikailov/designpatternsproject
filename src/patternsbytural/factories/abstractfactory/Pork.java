package patternsbytural.factories.abstractfactory;

public class Pork implements Meat{
    @Override
    public String toString() {
        return "Pork meat";
    }
}
