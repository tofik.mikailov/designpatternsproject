package patternsbytural.factories.abstractfactory;

public interface McdonaldsBurgerFactory {
    public abstract Meat createMeat();
    public abstract Cheese createCheese();
    public abstract Veggies[] createVeggies();
}
