package patternsbytural.factories.abstractfactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Burger {
    protected String name;
    protected String sauce="Mcdonalds Special Sauce";
    protected Cheese cheese;
    protected Meat meat;
    protected Veggies[] veggies;

    public void setName(String name) {
        this.name = name;
    }

    public abstract void prepareBurger();

    public void wrapBurger() {
        System.out.println("Wrapping " + name);
    }

    @Override
    public String toString() {
        return name + sauce;
    }

    public String getName() {
        return name;
    }
    public void dislay(){
        System.out.println("Your order "+getName()+" ready! Bone Appetit!!!");
    }
}
