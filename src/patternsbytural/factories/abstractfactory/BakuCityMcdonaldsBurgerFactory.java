package patternsbytural.factories.abstractfactory;

public class BakuCityMcdonaldsBurgerFactory implements McdonaldsBurgerFactory {
    @Override
    public Meat createMeat() {
        return new Beef();
    }

    @Override
    public Cheese createCheese() {
        return new MozarellaCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies={new Lettuce(),new Tomato(),new Pickles()};
        return veggies;
    }
}
